import { Component, OnInit, ViewChild } from '@angular/core';
import { SwiperConfigInterface } from 'src/app/Shared/components/swiper/swiper.interfaces';
import { WeatherService } from '../../weather.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { City } from '../../models/city.model';
import { ModalComponent } from '../../../shared/components/modal/modal.component';
import { Chart } from "chart.js";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  @ViewChild('cardModal',{static: false}) modalRef: ModalComponent;
  cities: Array<City>;
  citySelected: City;
  chart = [];

  slides = [
    'Bogota',
    'London',
    'Miami',
    'Barcelona',
    'Berlin'
  ];

  config: SwiperConfigInterface = {
    a11y: true,
    width: 980,
    height: 100,
    direction: 'horizontal',
    slidesPerView: 3,
    spaceBetween: 15,
    slidesOffsetBefore: 15,
    slidesOffsetAfter: 15,
    centerInsufficientSlides: true,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: false,
    pagination: false
  };

  loader:boolean;


  constructor(private weatherService: WeatherService, public dialog: MatDialog) { }


  ngOnInit() {
    this.cities = new Array<City>();
    this.getWeatherByCityName();
  }

  openDialog(event): void {
    this.citySelected = event;
    this.generateChart();
  }

  getTitleModal() {
    return this.citySelected == undefined ? '' : `${this.citySelected.name} Forecast`;
  }

  getWeatherByCityName() {
    this.loader = true;
    this.slides.forEach(city=>{
      this.weatherService.getCityWeather(city).subscribe(response => {
        this.cities.push(response);
      });
    });
    this.loader = false;
  }

  generateChart() {
    this.weatherService.daysWeather(this.citySelected.name,5).subscribe(res => {
      const temp_max = res.map(res => res.temp.max);
      const temp_min = res.map(res => res.temp.min);
      const allDays = res.map(res => res.dt);
      const weahterDays = [];
      let options = {
        weekday: "long",
        month: "short",
        day: "numeric"
      };

      allDays.forEach(result => {
        weahterDays.push(new Date(result * 1000).toLocaleTimeString("es", options));
      });

      this.modalRef.openDialog();

      this.chart = new Chart("canvas", {
        type: "bar",
        data: {
          labels: weahterDays,
          datasets: [
            {
              label: "Max Temperature",
              data: temp_max,
              backgroundColor: [
                "red",
                "red",
                "red",
                "red",
                "red",
                "red",
                "red"
              ],
              fill: false
            },
            {
              label: "Min Temperature",
              data: temp_min,
              backgroundColor: [
                "#00ffff",
                "#00ffff",
                "#00ffff",
                "#00ffff",
                "#00ffff",
                "#00ffff",
                "#00ffff"
              ],
              fill: false
            }
          ]
        },
        options: {
          maintainAspectRatio: false,
          legend: {
            dispaly: false
          },
          scales: {
            xAxes: [
              {
                display: true
              }
            ],
            yAxes: [
              {
                display: true
              }
            ]
          }
        }
      });

      

    });
  }

  closeModal() {
    this.modalRef.closeDialog();
  }

}
