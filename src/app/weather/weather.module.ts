import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './components/weather/weather.component';
import { SharedModule } from '../shared/shared.module';
import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherService } from './weather.service';

@NgModule({
  declarations: [
      WeatherComponent
  ],
  imports: [
    WeatherRoutingModule,
    CommonModule,
    SharedModule
  ],
  exports: [
    WeatherComponent
  ],
  providers: [
    WeatherService
  ]
})
export class WeatherModule { }
