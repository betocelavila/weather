export interface WeatherCity {
  description: string,
  icon: string,
  id: number,
  main: string,
}
  
export interface WeatherMainCity {
  humidity: number,
  pressure: number,
  temp: number,
  temp_max: number,
  temp_min: number,
  average_temp: number,
}

export interface WindCity {
  deg: number,
  speed: number,
}

export interface City {
  country: string;
  name: string,
  weatherMainCity: WeatherMainCity,
  weatherCity: WeatherCity,
  windCity: WindCity,
}

