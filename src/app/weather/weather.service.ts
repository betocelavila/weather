import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map, mapTo, flatMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { City } from './models/city.model';
import { Observable } from 'rxjs';


@Injectable()
export class WeatherService {
  
  private apiUrlWeatherbase = 'https://api.openweathermap.org/data/2.5/weather?';
  private apiUrlForecastBase = 'https://api.openweathermap.org/data/2.5/forecast/daily?';
  
  constructor(private _http: HttpClient) {}

  public c: City;
  daysWeather(cityName:string, days:number, ) {
    const url = this.apiUrlForecastBase + `q=${cityName}&cnt=${days}&units=metric&appid=${environment.appid}`;
    return this._http.get<any>(url).pipe(map(res => {
      return res.list;
    }));
  }

  getCityWeather(cityName:string): Observable<City> {
    const url = this.apiUrlWeatherbase + `q=${cityName}&units=metric&appid=${environment.appid}`;
    return this._http.get<any>(url).pipe(
      map(res=>{
        let city:City = {
          name: res.name,
          weatherCity: res.weather[0],
          weatherMainCity: res.main,
          windCity: res.wind,
          country: res.sys.country.toLowerCase()
        };
        city.weatherMainCity.average_temp = Math.round((res.main.temp_min + res.main.temp_max)/2);
        return city;
      })
    );
  }

}