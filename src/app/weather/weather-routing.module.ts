import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherComponent } from './components/weather/weather.component';

export const routes: Routes = [

    {
        path: '',
        component: WeatherComponent,
    },

];


export const WeatherRoutingModule = RouterModule.forChild(routes);
