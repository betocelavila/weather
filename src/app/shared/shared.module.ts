import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatInputModule, MatDialogModule, MatButtonModule, MatListModule, MatProgressSpinnerModule } from '@angular/material'
import { SwiperModule } from './components/swiper/swiper.module';
import { FormsModule } from '@angular/forms';
import { ModalComponent } from './components/modal/modal.component';
import { CardComponent } from './components/card/card.component';
// import { CardComponent } from './components/card/card.component';

@NgModule({
  declarations: [
    ModalComponent,
    CardComponent
  ],
  imports: [
    FormsModule,
    SwiperModule,
    MatCardModule,
    MatButtonModule,
    CommonModule,
    MatInputModule,
    MatDialogModule,
    MatListModule,
    MatProgressSpinnerModule
  ],
  exports: [
    ModalComponent,
    CardComponent,

    SwiperModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatListModule,
    MatProgressSpinnerModule
  ]
})
export class SharedModule { }
