import { OnInit, Component, ViewEncapsulation, ElementRef, ViewChild, Input, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
selector: 'app-modal',
templateUrl: './modal.component.html',
styleUrls: ['./modal.component.css'],
})
export class ModalComponent { 

  @ViewChild('myDialog', {static: false}) myDialogRef: any
  @Input() title: string;
  @Input() width: string = '550px';
  dialogRef: MatDialogRef<unknown, any>

  constructor(private dialog: MatDialog){}

  openDialog(): void {
      this.dialogRef = this.dialog.open(this.myDialogRef, {
        width: this.width,
        data: {}
      });

      this.dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        // this.animal = result;
      });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}