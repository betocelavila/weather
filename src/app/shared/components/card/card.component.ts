import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
selector: 'app-card',
templateUrl: './card.component.html',
styleUrls: ['./card.component.css'],
})
export class CardComponent { 

  @Output() onClickCard = new EventEmitter<any>();
  @Input() data: any;

  constructor(){}

  onClick(): void {
      this.onClickCard.emit(this.data);
  }
}